package com.library.raddiwala.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import android.widget.Filter;
import android.widget.Filterable;
import androidx.recyclerview.widget.RecyclerView;

import com.library.raddiwala.R;
import com.library.raddiwala.pojo;

import java.util.ArrayList;

public class AdapterRecyclerLinear extends RecyclerView.Adapter<AdapterRecyclerLinear.MyHolder> {

    Context context;
    ArrayList<pojo> pojoLinears;


    public AdapterRecyclerLinear(Context context,ArrayList<pojo> pojoLinears) {
        this.context = context;
        this.pojoLinears = pojoLinears;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.custom_list, viewGroup, false);

        MyHolder myHolder = new MyHolder(view);
        return myHolder;
    }

    @Override
    public void onBindViewHolder(MyHolder myHolder, int i) {

        myHolder.codeName.setText(pojoLinears.get(i).getCodeName());
        myHolder.codeVersion.setText(pojoLinears.get(i).getCodeversion());
    }

    @Override
    public int getItemCount() {
        return pojoLinears.size();
    }

    public void filteredList(ArrayList<pojo> filteredList){
         pojoLinears=filteredList;
         notifyDataSetChanged();
    }

    public static class MyHolder extends RecyclerView.ViewHolder {
        TextView codeName, codeVersion;

        public MyHolder(View itemView) {
            super(itemView);

            codeName = itemView.findViewById(R.id.code_name);
            codeVersion = itemView.findViewById(R.id.code_version);
        }
    }
}
