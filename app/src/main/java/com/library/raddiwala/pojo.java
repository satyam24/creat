package com.library.raddiwala;

public class pojo {
    private String codeName;
    private String codeversion;

    public pojo(String codeName, String codeversion) {
        this.codeName = codeName;
        this.codeversion = codeversion;
    }

    public String getCodeName() {
        return codeName;
    }

    public String getCodeversion() {
        return codeversion;
    }
}
