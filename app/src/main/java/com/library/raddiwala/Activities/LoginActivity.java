package com.library.raddiwala.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.library.raddiwala.DataBaseHelper;
import com.library.raddiwala.R;

public class LoginActivity extends AppCompatActivity {

    Button btn1,btn2;
    TextView text;
    EditText et1,et2;
    DataBaseHelper Db;
    Boolean check;

    String fn,ln,mob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Db=new DataBaseHelper(this);

        et1=findViewById(R.id.uname);
        et2=findViewById(R.id.password);
        btn1=findViewById(R.id.loginbtn);
        btn2=findViewById(R.id.signupbtn);
        text=findViewById(R.id.forgot);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s1=et1.getText().toString().trim();
                String s2=et2.getText().toString();



                if (s1.equals("")|s2.equals("")) {
                    Toast.makeText(getApplicationContext(),"Feilds Empty",Toast.LENGTH_LONG).show();
                }
                else {
                    check=Db.checkmate(s1,s2);
                    if (check==true) {
                        et1.setText("");
                        et2.setText("");
                        try {
                            long l=Long.parseLong(s1);
                            fn=Db.getFName(l);
                            ln=Db.getlName(l);
                            mob=Db.getMobile(l);
                        }
                        catch (Exception e){

                        }

                        Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);

                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "User does not Exist", Toast.LENGTH_LONG).show();
                    }
                }
                }

        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
                finish();

            }
        });

        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LoginActivity.this, PasswordActivity.class);
                startActivity(intent);
                finish();

            }
        });

    }
}
