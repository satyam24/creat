package com.library.raddiwala.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import com.google.android.material.navigation.NavigationView;
import com.library.raddiwala.Adapter.AdapterRecyclerLinear;
import com.library.raddiwala.R;
import com.library.raddiwala.pojo;
import java.util.ArrayList;
import java.util.logging.Filter;

public class DashboardActivity extends AppCompatActivity  //implements NavigationView.OnNavigationItemSelectedListener
{

    Toolbar toolbar;
   DrawerLayout drawerLayout;
    NavigationView navigationView;
    TextView textView1,textView2,textView3,textView4;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    AdapterRecyclerLinear adapterRecyclerLinear;
    ArrayList<pojo> list;
    EditText search;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dashboard);
		linearList();

		toolbar = findViewById(R.id.appbar);
		setSupportActionBar(toolbar);
		toolbar.setTitle("Dashboard");


		search = findViewById(R.id.search_bar);
		recyclerView = findViewById(R.id.Rview);
		recyclerView.setHasFixedSize(true);
		layoutManager = new LinearLayoutManager(this);
		recyclerView.setLayoutManager(layoutManager);
		adapterRecyclerLinear = new AdapterRecyclerLinear(this, list);
		recyclerView.setAdapter(adapterRecyclerLinear);

		search.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				filter(s.toString());
			}
		});

	}
	   public void filter(String text){
    	ArrayList<pojo> filtererdList= new ArrayList<>();

    	for(pojo item:list){
            if(item.getCodeName().toLowerCase().contains(text.toLowerCase())){
            	filtererdList.add(item);
			}
		}
    	adapterRecyclerLinear.filteredList(filtererdList);
	   }



                      private void linearList(){

            	        list = new ArrayList<>();
            	        list.add(new pojo("Pie","9.0"));
            	        list.add(new pojo("Oreo","8.0"));
            	        list.add(new pojo("Nougat","7.0"));
            	        list.add(new pojo("Marshmallow","6.0"));
            	        list.add(new pojo("Lollipop","5.0"));
            	        list.add(new pojo("Kitkat","4.4"));
            	        list.add(new pojo("Jelly Bean","4.1"));
            	        list.add(new pojo("Ice cream sandwich","4.0"));
            	        list.add(new pojo("Honey Comb","3.0 , 3.1 and 3.2"));
            	        list.add(new pojo("Gingerbread","2.3 , 2.4"));
            	        list.add(new pojo("Froyo","2.2"));
                        list.add(new pojo("Eclair","2.0 , 2.1"));
            	        list.add(new pojo("Donut","1.6"));
            	        list.add(new pojo("Cupcake","1.5"));
            	        list.add(new pojo("Unnamed and petite four","1.0 , 1.1"));






}

        }
    //drawerLayout = findViewById(R.id.drawerLayout);
      //  NavigationView navigationView=findViewById(R.id.navigation);
        //navigationView.setNavigationItemSelectedListener(this);
     //   ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);
       // drawerLayout.addDrawerListener((toggle));
        //toggle.syncState();

     //   if (savedInstanceState==null){
       //     getSupportFragmentManager().beginTransaction().replace(R.id.framer,new HomeFragment()).commit();
         //   navigationView.setCheckedItem(R.id.home);}




 /*
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()){
            case R.id.home:
                getSupportFragmentManager().beginTransaction().replace(R.id.framer,new HomeFragment()).addToBackStack(null).commit();
                break;
            case R.id.FAQ:
                getSupportFragmentManager().beginTransaction().replace(R.id.framer,new help_fragment()).addToBackStack(null).commit();
                break;
            case R.id.Favorites:
                getSupportFragmentManager().beginTransaction().replace(R.id.framer,new Fav_fragment()).addToBackStack(null).commit();break;

            case R.id.Settings:
                getSupportFragmentManager().beginTransaction().replace(R.id.framer,new settings_Fragment()).addToBackStack(null).commit();
                break;
            case R.id.logout:
                Intent intent=new Intent(DashboardActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.order:
                getSupportFragmentManager().beginTransaction().replace(R.id.framer,new orderfrag()).addToBackStack(null).commit();
                break;
            case R.id.share:

                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        else{
            super.onBackPressed();
        }
    }
*/


