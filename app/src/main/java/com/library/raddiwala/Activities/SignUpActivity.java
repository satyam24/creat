package com.library.raddiwala.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.library.raddiwala.DataBaseHelper;
import com.library.raddiwala.R;

public class SignUpActivity extends AppCompatActivity {

    private static final String TAG = "";
    private static final int RC_SIGN_IN =1;
    Button submit;
    DataBaseHelper Db=new DataBaseHelper(this);
    EditText fname,lname,pass,mob,check,email;
    GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

         mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        // Set the dimensions of the sign-in button.
        SignInButton signInButton = findViewById(R.id.google_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    switch (v.getId()) {
                        case R.id.google_button:
                            signIn();
                            Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                            break;
                        // ...

                }
            }
        });
        fname=findViewById(R.id.fname);
        lname=findViewById(R.id.lname);
        mob=findViewById(R.id.cell);
        pass=findViewById(R.id.pass);
        check=findViewById(R.id.cnf);
        email=findViewById(R.id.email);
        submit=findViewById(R.id.submit);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String s1=fname.getText().toString();
                String s2=lname.getText().toString();
                String s3=mob.getText().toString();
                String s4=pass.getText().toString();
                String s5=check.getText().toString();
                String s6=email.getText().toString();

                if (s1.equals("")|s2.equals("")|s3.equals("")|s4.equals("")|s6.equals("")){
                    Toast.makeText(getApplicationContext(),"Feilds Empty",Toast.LENGTH_LONG).show();
                }else {
                    if (s4.equals(s5)){
                       Boolean ins= Db.insertdata(s1,s2,s3,s4,s6);
                       if (ins){
                    fname.setText("");
                    lname.setText("");
                    mob.setText("");
                    pass.setText("");
                    check.setText("");
                    email.setText("");


                    Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();}
                }
                    else{
                        Toast.makeText(getApplicationContext(),"PassWord Don't Match",Toast.LENGTH_LONG).show();
                    }
                }

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        updateUI(account);
    }

    private void updateUI(GoogleSignInAccount account) {
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }
}
