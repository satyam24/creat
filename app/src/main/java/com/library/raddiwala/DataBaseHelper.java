package com.library.raddiwala;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.CancellationSignal;
import android.text.Editable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DataBaseHelper extends SQLiteOpenHelper {


    private static final String COL1="First_Name";
    private static final String COL2="Last_Name";
    private static final String COL3="Mobile_Number";
    private static final String COL4="Password";
    private static final String COL5="Email_ID";



    public DataBaseHelper(@Nullable Context context) {
        super(context, "APP.db", null, 3);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " +"userdata"+ "("+ COL1 + " TEXT,"+ COL2 + " TEXT,"+ COL3 + " INTEGER,"+ COL4 + " TEXT,"+ COL5 + " TEXT PRIMARY KEY )");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS userdata" );

    }

    public Boolean insertdata(String fn, String ln, String cell, String code, String mail) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("First_Name",fn);
        contentValues.put("Last_Name",ln);
        contentValues.put("Mobile_Number",cell);
        contentValues.put("Password",code);
        contentValues.put("Email_ID",mail);
        long ins=db.insert("userdata", null, contentValues);
        
                 if (ins==-1)
                 return false;
                 else
                     return true;

    }


    public Boolean checkmate(String email,String pass) {
        SQLiteDatabase db=this.getReadableDatabase() ;
           Cursor cursor = db.rawQuery("Select * from userdata where Email_Id=? and Password=?",new String[]{email,pass});
          
        if (cursor.getCount()>0){
             return  true;
        }
        else
            return false;
    }

    public String getFName(long l1) {
                SQLiteDatabase db=this.getReadableDatabase();
                String[] columns=new String[]{COL1,COL2,COL3,COL5};
        	        Cursor cursor=db.query("userdata",columns,COL5+"="+l1,null,null,null,null);
        	        if(cursor!=null){
            	            cursor.moveToFirst();
            	            String name=cursor.getString(1);
            	            return name;
            	        }
        	        return null;
        	    }
    public String getlName(long l1) {
        SQLiteDatabase db=this.getReadableDatabase();
        String[] columns=new String[]{COL1,COL2,COL3,COL5};
        Cursor cursor=db.query("userdata",columns,COL5+"="+l1,null,null,null,null);
        if(cursor!=null){
            cursor.moveToFirst();
            String name=cursor.getString(2);
            return name;
        }
        return null;
    }

        	    public String getMobile(long l1) {
        	        SQLiteDatabase db=this.getReadableDatabase();
        	        String[] columns=new String[]{COL1,COL2,COL3,COL5};
        	        Cursor cursor=db.query("userdata",columns,COL5+"="+l1,null,null,null,null);
        	    if(cursor!=null){
                cursor.moveToFirst();
            	            String name=cursor.getString(3);
            	            return name;
            	        }
        	        return null;
        	    }
}



